export default {
  // 有值就取前面的，没值就取后面的空数组
  cart: JSON.parse(localStorage.getItem('cart')) || []
}
