export default {
  // 从state里拿到cart，在接受的同时解构
  totalMoney ({ cart }) {
    // 算总价用reduce，把数组中通过回调函数返回成一个值
    return cart.reduce((money, shop) => {
      // 选中的才算总价
      if (shop.check) {
        money += shop.price * shop.count
      }
      return money
    }, 0)
  },
  isAllCheck ({ cart }) {
    // every() 方法用于检测数组所有元素是否都符合指定条件（通过函数提供）
    return cart.length === 0 ? false : cart.every(shop => shop.check)
  },
  totalCount ({ cart }) {
    return cart.reduce((AllCount, shop) => {
      // 选中的才算总价
      AllCount += shop.count
      return AllCount
    }, 0)
  }
}
