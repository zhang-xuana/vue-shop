import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import state from './state'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

// 插件：每当mutation被提交，插件的代码就会执行
const cartPlugin = store => {
  store.subscribe((mutation, state) => {
    // 每当有mutation被提交了，就会执行这个方法
    // 把state.cart存入localStorage
    localStorage.setItem('cart', JSON.stringify(state.cart))
  })
}

export default new Store({
  plugins: [cartPlugin],
  state,
  mutations,
  getters
})
