export default {
  // payload,提交载荷，现在的payload就是Detail那里传过来的detail
  // 在项目中，payload一般是传对象，这样更加的语义化
  addToCart (state, { detail }) {
    // 添加购物车
    // 看存不存在用some
    // some会遍历数组中的每个元素，让每个值都执行一遍callback函数
    // 如果有一个元素满足条件，返回true , 剩余的元素不会再执行检测。
    // 如果没有满足条件的元素，则返回false。
    // some不会改变原数组
    const isExist = state.cart.some(shop => shop.id === detail.id)
    if (isExist) {
      state.cart = state.cart.map(shop => {
        if (shop.id === detail.id) shop.count++
        return shop
      })
    } else {
      // 不存在把payload  push进去就行了，再加上count和check
      state.cart.push({
        ...detail,
        count: 1,
        check: true
      })
    }
  },
  toggleCheck (state, { id }) {
    state.cart = state.cart.map(shop => {
      if (shop.id === id) shop.check = !shop.check
      return shop
    })
  },
  changeCountById (state, { id, num }) {
    state.cart = state.cart.map(shop => {
      if (shop.id === id) shop.count = num
      return shop
    })
  },
  delShop (state, { id }) {
    console.log(id)
    state.cart = state.cart.filter(shop => shop.id !== id)
  },
  toggleAllCheck (state, { isAllCheck }) {
    state.cart = state.cart.map(shop => {
      shop.check = !isAllCheck
      return shop
    })
  }
}
