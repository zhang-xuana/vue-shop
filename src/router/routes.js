import Home from '@/views/Home'
import ZxFooter from '@/components/ZxFooter'

const Mall = () => import('@/views/Mall')
const ZxCategory = () => import('@/components/ZxCategory')
const Cart = () => import('@/views/Cart')
const Mine = () => import('@/views/Mine')
const Detail = () => import('@/views/Detail')

const routes = [
  {
    path: '/',
    redirect: '/home',
    meta: {
      isTabbar: false
    }
  },
  {
    path: '/home',
    name: 'Home',
    components: {
      default: Home,
      'zx-footer': ZxFooter
    },
    meta: {
      isTabbar: true,
      title: '首页',
      icon: 'home-o'
    }
  },
  {
    path: '/mall',
    name: 'Mall',
    components: {
      default: Mall,
      'zx-footer': ZxFooter
    },
    meta: {
      isTabbar: true,
      title: '商城',
      icon: 'shop-o'
    },
    children: [
      {
        path: ':id',
        component: ZxCategory
      }
    ]
  },
  {
    path: '/cart',
    name: 'Cart',
    components: {
      default: Cart,
      'zx-footer': ZxFooter
    },
    meta: {
      isTabbar: true,
      title: '购物车',
      icon: 'shopping-cart-o'
    }
  },
  {
    path: '/mine',
    name: 'Mine',
    components: {
      default: Mine,
      'zx-footer': ZxFooter
    },
    meta: {
      isTabbar: true,
      title: '我的',
      icon: 'friends-o'
    }
  },
  {
    path: '/detail',
    name: 'Detail',
    component: Detail,
    meta: {
      isTabbar: false
    }
  }
]

export default routes
