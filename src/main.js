import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vant from 'vant'
import 'vant/lib/index.css'
// 如果模块里分开导入多个属性或者方法，那么导入的时候需要使用 * as ,就会把所有导出的 属性方法放入ajax对象
import * as ajax from './request'

Vue.prototype.$http = ajax
Vue.use(vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
